package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String,Double> priceList;
    public Quoter(){
        priceList = new HashMap<>();
        priceList.put("1", 10.0);
        priceList.put("2", 45.0);
        priceList.put("3", 20.0);
        priceList.put("4", 35.0);
        priceList.put("5", 50.0);
    }
    public double getBookPrice(String isbn){
        return priceList.getOrDefault(isbn, 0.0);
    }

}
